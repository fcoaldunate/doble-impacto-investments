import Vue from 'vue'
import Router from 'vue-router'
import Inversionista from '@/components/Inversionista'
import BootstrapVue from 'bootstrap-vue'
import vueSmoothScroll from 'vue-smooth-scroll'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Router)
Vue.use(BootstrapVue)
Vue.use(vueSmoothScroll)

export default new Router({
  routes: [
    {
      path: '/inversionista',
      name: 'Inversionista',
      component: Inversionista
    }
  ]
})
